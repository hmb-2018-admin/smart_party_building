package com.ruoyi.project.module.article.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 分享美文对象 tb_article
 * 
 * @author ruoyi
 * @date 2020-05-09
 */
public class Article extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long articleId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    private String deptName;

    /** 提交人id */
    @Excel(name = "提交人id")
    private Long userId;

    /** 提交人姓名 */
    @Excel(name = "提交人姓名")
    private String userName;

    /** 分享时间 */
    @Excel(name = "分享时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date articleTime;

    /** 美文标题 */
    @Excel(name = "美文标题")
    private String articleTitle;

    /** 美文内容 */
    @Excel(name = "美文内容")
    private String articleContent;

    /** 美文图片 */
    @Excel(name = "美文图片")
    private String articleImg;

    /** 美文视频 */
    @Excel(name = "美文视频")
    private String articleVideo;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setArticleId(Long articleId) 
    {
        this.articleId = articleId;
    }

    public Long getArticleId() 
    {
        return articleId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setArticleTime(Date articleTime) 
    {
        this.articleTime = articleTime;
    }

    public Date getArticleTime() 
    {
        return articleTime;
    }
    public void setArticleTitle(String articleTitle) 
    {
        this.articleTitle = articleTitle;
    }

    public String getArticleTitle() 
    {
        return articleTitle;
    }
    public void setArticleContent(String articleContent) 
    {
        this.articleContent = articleContent;
    }

    public String getArticleContent() 
    {
        return articleContent;
    }
    public void setArticleImg(String articleImg) 
    {
        this.articleImg = articleImg;
    }

    public String getArticleImg() 
    {
        return articleImg;
    }
    public void setArticleVideo(String articleVideo) 
    {
        this.articleVideo = articleVideo;
    }

    public String getArticleVideo() 
    {
        return articleVideo;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("articleId", getArticleId())
            .append("deptId", getDeptId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("articleTime", getArticleTime())
            .append("articleTitle", getArticleTitle())
            .append("articleContent", getArticleContent())
            .append("articleImg", getArticleImg())
            .append("articleVideo", getArticleVideo())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
