package com.ruoyi.project.module.news.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 资讯对象 tb_news
 * 
 * @author ruoyi
 * @date 2020-05-06
 */
public class News extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long newId;

    /** 资讯标题 */
    @Excel(name = "资讯标题")
    private String newTitle;

    /** 封面图 */
    @Excel(name = "封面图")
    private String newPic;

    /** 资讯分类id */
    @Excel(name = "资讯分类id")
    private String newTypeId;
    private String typeName;

    /** 上传至部门id */
    @Excel(name = "上传至部门id")
    private Long deptId;

    private String deptName;

    /** 资讯内容 */
    @Excel(name = "资讯内容")
    private String newContent;

    /** 上传文件地址 */
    @Excel(name = "上传文件地址")
    private String newFile;

    /** 上传视频地址 */
    @Excel(name = "上传视频地址")
    private String newVideo;

    /** 审核人id */
    @Excel(name = "审核人id")
    private Long userId;

    /** 审核人名称 */
    @Excel(name = "审核人名称")
    private String userName;

    /** 审核状态(1 待审核 2 审核通过  ) */
    @Excel(name = "审核状态(1 待审核 2 审核通过  )")
    private String status;

       /** 类型(1资讯 2在线学习) */
    @Excel(name = "类型(1资讯 2在线学习)")
    private String type;
	
    private String learnStatus;

    public void setNewId(Long newId) 
    {
        this.newId = newId;
    }

    public Long getNewId() 
    {
        return newId;
    }
    public void setNewTitle(String newTitle) 
    {
        this.newTitle = newTitle;
    }

    public String getNewTitle() 
    {
        return newTitle;
    }
    public void setNewPic(String newPic) 
    {
        this.newPic = newPic;
    }

    public String getNewPic() 
    {
        return newPic;
    }
    public void setNewTypeId(String newTypeId) 
    {
        this.newTypeId = newTypeId;
    }

    public String getNewTypeId() 
    {
        return newTypeId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setNewContent(String newContent) 
    {
        this.newContent = newContent;
    }

    public String getNewContent() 
    {
        return newContent;
    }
    public void setNewFile(String newFile) 
    {
        this.newFile = newFile;
    }

    public String getNewFile() 
    {
        return newFile;
    }
    public void setNewVideo(String newVideo) 
    {
        this.newVideo = newVideo;
    }

    public String getNewVideo() 
    {
        return newVideo;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getLearnStatus() {
        return learnStatus;
    }

    public void setLearnStatus(String learnStatus) {
        this.learnStatus = learnStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("newId", getNewId())
            .append("newTitle", getNewTitle())
            .append("newType", getNewTypeId())
            .append("deptId", getDeptId())
            .append("newContent", getNewContent())
            .append("newFile", getNewFile())
            .append("newVideo", getNewVideo())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
