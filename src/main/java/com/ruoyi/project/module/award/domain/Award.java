package com.ruoyi.project.module.award.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 奖项对象 tb_award
 * 
 * @author ruoyi
 * @date 2020-05-08
 */
public class Award extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long awardId;

    /** 奖项标题 */
    @Excel(name = "奖项标题")
    private String awardTitle;

    /** 封面图 */
    @Excel(name = "封面图")
    private String awardPic;

    /** 奖项内容 */
    @Excel(name = "奖项内容")
    private String awardContent;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setAwardId(Long awardId) 
    {
        this.awardId = awardId;
    }

    public Long getAwardId() 
    {
        return awardId;
    }
    public void setAwardTitle(String awardTitle) 
    {
        this.awardTitle = awardTitle;
    }

    public String getAwardTitle() 
    {
        return awardTitle;
    }
    public void setAwardPic(String awardPic) 
    {
        this.awardPic = awardPic;
    }

    public String getAwardPic() 
    {
        return awardPic;
    }
    public void setAwardContent(String awardContent) 
    {
        this.awardContent = awardContent;
    }

    public String getAwardContent() 
    {
        return awardContent;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("awardId", getAwardId())
            .append("awardTitle", getAwardTitle())
            .append("awardContent", getAwardContent())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
