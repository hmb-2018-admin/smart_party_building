package com.ruoyi.project.module.pageruser.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 考试对象对象 tb_pager_user
 * 
 * @author ruoyi
 * @date 2020-06-19
 */
public class PagerUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 考试对象 */
    @Excel(name = "考试对象")
    private Long userId;
    private String userName;

    /** 试卷id */
    @Excel(name = "试卷id")
    private Long pagerId;
    private String pagerName;

    private String pagerStatus;

    /** 得分 */
    @Excel(name = "得分")
    private Long mark;

    /** 状态（0未开始 1进行中2 考试结束） */
    @Excel(name = "状态", readConverterExp = "0=未开始,1=进行中2,考=试结束")
    private String status;

    /** 确认最后分数(0未确认 1已确认) */
    @Excel(name = "确认最后分数(0未确认 1已确认)")
    private String confirm;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setPagerId(Long pagerId) 
    {
        this.pagerId = pagerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getPagerId()
    {
        return pagerId;
    }

    public String getPagerName() {
        return pagerName;
    }

    public void setPagerName(String pagerName) {
        this.pagerName = pagerName;
    }

    public void setMark(Long mark)
    {
        this.mark = mark;
    }

    public Long getMark() 
    {
        return mark;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getPagerStatus() {
        return pagerStatus;
    }

    public void setPagerStatus(String pagerStatus) {
        this.pagerStatus = pagerStatus;
    }
	
	    public void setConfirm(String confirm) 
    {
        this.confirm = confirm;
    }

    public String getConfirm() 
    {
        return confirm;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("pagerId", getPagerId())
            .append("mark", getMark())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
