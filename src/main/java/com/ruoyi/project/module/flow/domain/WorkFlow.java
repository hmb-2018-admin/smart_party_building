package com.ruoyi.project.module.flow.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 任务流程对象 tb_work_flow
 * 
 * @author ruoyi
 * @date 2020-05-18
 */
public class WorkFlow extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long flowId;

    /** 任务id */
    @Excel(name = "任务id")
    private Long workId;

    private Long distId;

    /** 处理人id */
    @Excel(name = "处理人id")
    private Long dealId;

    /** 处理人姓名 */
    @Excel(name = "处理人姓名")
    private String dealName;

    /** 当前状态 */
    @Excel(name = "当前状态")
    private String currentStatus;

    /** 等级(1 执行人 2 验证人) */
    @Excel(name = "等级(1 执行人 2 验证人)")
    private String level;

    /** 执行状态(1 执行中 2 已完成 3不同意  4 同意)  */
    @Excel(name = "执行状态(1 执行中 2 已完成 3不同意  4 同意) ")
    private String dealStatus;

    /** 动作(1执行人执行 2 提交人验证) */
    @Excel(name = "动作(1执行人执行 2 提交人验证)")
    private String actionName;

    /** 执行说明 */
    @Excel(name = "执行说明")
    private String dealExplain;

    /** 执行时间 */
    @Excel(name = "执行时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dealTime;

    public Long getDistId() {
        return distId;
    }

    public void setDistId(Long distId) {
        this.distId = distId;
    }

    public void setFlowId(Long flowId)
    {
        this.flowId = flowId;
    }

    public Long getFlowId() 
    {
        return flowId;
    }
    public void setWorkId(Long workId) 
    {
        this.workId = workId;
    }

    public Long getWorkId() 
    {
        return workId;
    }
    public void setDealId(Long dealId) 
    {
        this.dealId = dealId;
    }

    public Long getDealId() 
    {
        return dealId;
    }
    public void setDealName(String dealName) 
    {
        this.dealName = dealName;
    }

    public String getDealName() 
    {
        return dealName;
    }
    public void setCurrentStatus(String currentStatus) 
    {
        this.currentStatus = currentStatus;
    }

    public String getCurrentStatus() 
    {
        return currentStatus;
    }
    public void setLevel(String level) 
    {
        this.level = level;
    }

    public String getLevel() 
    {
        return level;
    }
    public void setDealStatus(String dealStatus) 
    {
        this.dealStatus = dealStatus;
    }

    public String getDealStatus() 
    {
        return dealStatus;
    }
    public void setActionName(String actionName) 
    {
        this.actionName = actionName;
    }

    public String getActionName() 
    {
        return actionName;
    }
    public void setDealExplain(String dealExplain) 
    {
        this.dealExplain = dealExplain;
    }

    public String getDealExplain() 
    {
        return dealExplain;
    }
    public void setDealTime(Date dealTime) 
    {
        this.dealTime = dealTime;
    }

    public Date getDealTime() 
    {
        return dealTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("flowId", getFlowId())
            .append("workId", getWorkId())
            .append("dealId", getDealId())
            .append("dealName", getDealName())
            .append("currentStatus", getCurrentStatus())
            .append("level", getLevel())
            .append("dealStatus", getDealStatus())
            .append("dealExplain", getDealExplain())
            .append("dealTime", getDealTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
