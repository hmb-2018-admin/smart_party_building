package com.ruoyi.project.vote.vote.service;

import java.util.List;
import com.ruoyi.project.vote.vote.domain.Vote;

/**
 * 选举任务Service接口
 * 
 * @author ruoyi
 * @date 2020-06-30
 */
public interface IVoteService 
{
    /**
     * 查询选举任务
     * 
     * @param id 选举任务ID
     * @return 选举任务
     */
    public Vote selectVoteById(Long id);

    /**
     * 查询选举任务列表
     * 
     * @param vote 选举任务
     * @return 选举任务集合
     */
    public List<Vote> selectVoteList(Vote vote);

    /**
     * 新增选举任务
     * 
     * @param vote 选举任务
     * @return 结果
     */
    public int insertVote(Vote vote);

    /**
     * 修改选举任务
     * 
     * @param vote 选举任务
     * @return 结果
     */
    public int updateVote(Vote vote);

    /**
     * 批量删除选举任务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteVoteByIds(String ids);

    /**
     * 删除选举任务信息
     * 
     * @param id 选举任务ID
     * @return 结果
     */
    public int deleteVoteById(Long id);
}
